#ifndef ASSIGNMENT_IMAGE_ROTATION_CLOSING_H
#define ASSIGNMENT_IMAGE_ROTATION_CLOSING_H

#include <stdio.h>

#include "../util/statuses.h"
#include "../image/image.h"

typedef enum write_status to_format(FILE* in, struct image const* image);

enum close_status {
    CLOSE_SUCCESS = 0,
    CLOSE_ERROR
};

enum close_status close_file(FILE** file, struct image* image, enum write_status* status, to_format t);

#endif //ASSIGNMENT_IMAGE_ROTATION_CLOSING_H
#include "opening.h"

enum open_status open_file(FILE** file, const char* path,
                           struct image* image, enum read_status* status, from_format f) {
    enum open_status result;
    *file = fopen(path,"rb+");
    if (*file) {
        result = OPENING_SUCCESS;
        *status = f(*file, image);
    }
    else {
        result = OPENING_ERROR;
        *status = OPENING_FAILED;
    }

    return result;
}
#ifndef ASSIGNMENT_IMAGE_ROTATION_OPENING_H
#define ASSIGNMENT_IMAGE_ROTATION_OPENING_H

#include <stdio.h>

#include "../util/statuses.h"
#include "../image/image.h"

typedef enum read_status from_format(FILE*, struct image*);

enum open_status {
    OPENING_SUCCESS = 0,
    OPENING_ERROR
};

enum open_status open_file(FILE** file, const char* path,
        struct image* image, enum read_status* status, from_format f);

#endif //ASSIGNMENT_IMAGE_ROTATION_OPENING_H

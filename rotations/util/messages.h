#ifndef ASSIGNMENT_IMAGE_ROTATION_MESSAGES_H
#define ASSIGNMENT_IMAGE_ROTATION_MESSAGES_H

#include "../formats/bmp.h"
#include "../util/opening.h"
#include "../util/closing.h"

static const char* const opening_mnemonic[] = {
    [OPENING_SUCCESS] = "Opening successful.",
    [OPENING_ERROR] = "Opening failed, error."
};

static const char* const reading_mnemonic[] = {
        [READ_OK] = "Reading successful.",
        [READ_INVALID_SIGNATURE] = "Reading failed. Invalid signature.",
        [READ_INVALID_BITS] = "Reading failed. Invalid bits.",
        [READ_INVALID_HEADER] = "Reading failed. Invalid header.",
        [OPENING_FAILED] = "Error. File was not opened correctly."
};

static const char* const writing_mnemonic[] = {
        [WRITE_OK] = "Writing successful.",
        [WRITE_ERROR] = "Writing failed."
};

static const char* const closing_mnemonic[] = {
        [CLOSE_SUCCESS] = "Closing successful.",
        [CLOSE_ERROR] = "Closing failed."
};

#endif //ASSIGNMENT_IMAGE_ROTATION_MESSAGES_H

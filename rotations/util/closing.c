#include "closing.h"

enum close_status close_file(FILE** file, struct image* image, enum write_status* status, to_format t) {
    enum close_status result;
    *status = t(*file, image);
    if (!fclose(*file)) {
        result = CLOSE_SUCCESS;
    }
    else {
        result = CLOSE_ERROR;
    }

    return result;
}
#include <stdlib.h>

#include "rotate.h"

struct image rotate( struct image const source ) {
    struct image result = create_image(source.height, source.width);
    for (size_t i = 0; i < source.height; i = i + 1) {
        for (size_t j = 0; j < source.width; j = j + 1) {
            *(result.data + (result.height - j - 1) * result.width + i) = *(source.data + i * source.width + j);
        }
    }
    return result;
}

#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H

#include "../image/image.h"

struct image rotate( struct image const source );

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATE_H

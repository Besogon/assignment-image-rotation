#include <stdio.h>

#include "util/closing.h"
#include "util/opening.h"
#include "actions/rotate.h"
#include "util/messages.h"

void show_usage() {
    fprintf(stderr, "Usage: rotations BMP_FILE_NAME\n");
}

void check_closing(enum close_status close) {
    if (close == CLOSE_SUCCESS) {
        fprintf(stdout, closing_mnemonic[close], "\n");
    }
    else {
        fprintf(stderr, closing_mnemonic[close], "\n");
    }
}

void check_writing(enum write_status write, enum close_status close) {
    if (write == WRITE_OK) {
        fprintf(stdout, writing_mnemonic[write], "\n");
        check_closing(close);
    }
    else {
        fprintf(stderr, writing_mnemonic[write], "\n");
    }
}

void check_reading(enum read_status read,
        enum write_status write, enum close_status close) {

    if (read == READ_OK) {
        fprintf(stdout, reading_mnemonic[read], "\n");
        check_writing(write, close);
    }
    else {
        fprintf(stderr, reading_mnemonic[read], "\n");
    }

}

void check_opening(enum open_status open,
        enum read_status read, enum write_status write,
                enum close_status close) {

    if (open == OPENING_SUCCESS) {
        fprintf(stdout, opening_mnemonic[open], "\n");
        check_reading(read, write, close);
    }
    else {
        fprintf(stderr, opening_mnemonic[open], "\n");
    }
}

void clean_data(struct image* previous, struct image* new) {
    destroy_image(previous);
    destroy_image(new);
}

int main( int argc, char** argv ) {
    if (argc != 2) show_usage();
    if (argc < 2) fprintf(stderr, "Not enough arguments \n" );
    if (argc > 2) fprintf(stderr, "Too many arguments \n" );

    enum open_status openStatus;
    enum read_status readStatus;
    enum write_status writeStatus;
    enum close_status closeStatus;

    struct image image;
    FILE* file = NULL;

    openStatus = open_file(&file, argv[1], &image, &readStatus, from_bmp);
    if (readStatus == READ_OK) {
        struct image newImage = rotate(image);
        closeStatus = close_file(&file, &newImage, &writeStatus, to_bmp);

        check_opening(openStatus, readStatus, writeStatus, closeStatus);
        clean_data(&image, &newImage);
    }
    else {
        fprintf(stderr, reading_mnemonic[readStatus], "\n");
    }

    return 0;
}


#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <inttypes.h>

struct pixel {
    uint8_t blue;
    uint8_t green;
    uint8_t red;
};

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel* data;
};

struct image create_image(uint64_t width, uint64_t height);

void destroy_image(struct image* image);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

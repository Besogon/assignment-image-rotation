#include <inttypes.h>
#include <stdlib.h>

#include "image.h"

struct image create_image(uint64_t width, uint64_t height) {
    struct image result = {.width = width, .height = height};
    result.data = malloc(sizeof(struct pixel) * width * height);
    return result;
}

void destroy_image(struct image* image) {
    image->width = 0;
    image->height = 0;
    free(image->data);
}
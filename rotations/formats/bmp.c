#include <stdbool.h>

#include "bmp.h"

#define BMP_SIGNATURE 0x4d42
#define COLOR_DEPTH 24

uint32_t get_padding(const uint64_t width) {
    uint32_t result;
    size_t width_size = sizeof(struct pixel) * width;
    if (width_size % 4 != 0) {
        result = 4 - (width_size % 4);
    }
    else {
        result = 0;
    }
    return result;
}

struct bmp_header bmp_header_create(struct image const* image) {
    size_t const header_size = sizeof(struct bmp_header);
    size_t const pixel_size = sizeof(struct pixel);

    struct bmp_header header = {0};
    header.bfType = BMP_SIGNATURE;
    header.biBitCount = COLOR_DEPTH;
    header.biWidth = image->width;
    header.biHeight = image->height;
    header.bOffBits = header_size;
    header.bfileSize = header_size +
                       (pixel_size * image->width + image->width % 4) * image->height;
    header.biSizeImage = image->width * image->height * pixel_size;
    header.biSize = 40;
    header.biPlanes = 1;
    header.bfReserved = 0;
    header.biCompression = 0;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    return header;
}

enum read_status from_bmp(FILE* input, struct image* image) {
    enum read_status result;
    struct bmp_header header = {0};
    bool is_correct = true;

    size_t const header_size = sizeof(struct bmp_header);
    size_t const pixel_size = sizeof(struct pixel);

    if (fread(&header, header_size, 1, input) < 1) {
        if (feof(input)) {
            result = READ_INVALID_BITS;
        }
        else {
            result = READ_INVALID_HEADER;
        }
        is_correct = false;
    }
    else if (fseek(input, header.bOffBits, SEEK_SET) != 0) {
        result = READ_INVALID_BITS;
        is_correct = false;
    }
    else if (header.bfType != BMP_SIGNATURE) {
        result = READ_INVALID_SIGNATURE;
        is_correct = false;
    }

    if (is_correct) {
        *image = create_image(header.biWidth, header.biHeight);
        const uint32_t padding = get_padding(image->width);
        const size_t height = (size_t) image->height;
        for (size_t i = 0; i < height; i = i + 1) {
            if (fread(image->data + i * image->width, pixel_size,
                    image->width, input) < pixel_size) {
                result = READ_INVALID_BITS;
                is_correct = false;
            }
            if (fseek(input, padding, SEEK_CUR) != 0) {
                result = READ_INVALID_BITS;
                is_correct = false;
            }
        }
        if (fseek(input, 0, SEEK_SET) != 0) {
            result = READ_INVALID_BITS;
            is_correct = false;
        }
    }

    if (is_correct) result = READ_OK;
    else destroy_image(image); // Зачистка памяти в случае неудачи

    return result;
}

enum write_status to_bmp(FILE* output, struct image const* image) {
    enum write_status result;
    const struct bmp_header header = bmp_header_create(image);
    bool is_correct = true;

    const size_t header_size = sizeof(struct bmp_header);
    const size_t pixel_size = sizeof(struct pixel);

    if (fwrite(&header, header_size, 1, output) < 1) {
        is_correct = false;
    }

    const char zero_bytes[3] = {0};
    const uint32_t padding = get_padding(image->width);
    const size_t height = (size_t) image->height;

    for (size_t i = 0; i < height; i = i + 1) {
        if (fwrite(image->data + i * image->width, pixel_size,
                image->width, output) != image->width) {
            is_correct = false;
        }
        if (fwrite(zero_bytes, padding, 1, output) != 1 && padding != 0) {
            is_correct = false;
        }
        if (fflush(output) != 0) {
            is_correct = false;
        }
    }
    if (fseek(output, 0, SEEK_SET) != 0) is_correct = false;

    if (is_correct) {
        result = WRITE_OK;
    }
    else {
        result = WRITE_ERROR;
    }

    return result;

}


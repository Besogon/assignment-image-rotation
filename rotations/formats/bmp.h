#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include <stdint.h>
#include <stdio.h>

#include "../image/image.h"
#include "../util/statuses.h"

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

uint32_t get_padding(const uint64_t width);

struct bmp_header bmp_header_create(struct image const* image);

enum read_status from_bmp(FILE* in, struct image* image);

enum write_status to_bmp(FILE* out, struct image const* image);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
